import uuid
import asyncio
from http.cookies import BaseCookie
from aiohttp.client_reqrep import ClientResponse
from aiohttp import ClientSession


async def get_csrftoken() -> str:
    CSRF_URL: str = "http://127.0.0.1:8888/api/v1/user/csrftoken"

    async with ClientSession() as session:
        response: ClientResponse = await session.get(CSRF_URL)
        csrftoken: str = await response.json()
        return csrftoken


async def login() -> BaseCookie:
    LOGIN_URL: str = "http://127.0.0.1:8888/api/v1/user/login"
    LOGIN_DATA: dict = {"username": "test", "password": "test123"}

    csrftoken = await get_csrftoken()

    async with ClientSession(cookies={"csrftoken": csrftoken}, headers={"X-Csrftoken": csrftoken}) as session:
        response: ClientResponse = await session.post(LOGIN_URL, json=LOGIN_DATA)
        data = await response.text()
        print(f"login cookies: {response.cookies}")
        print(f"login response: {data}")
        return response.cookies


async def task(session: ClientSession):    
    COMMODITY_URL: str = 'http://127.0.0.1:8888/api/v1/store/commodities'
    COMMODITY_DATA = []
    for i in range(3):
        COMMODITY_DATA.append({
            'name': str(uuid.uuid4()),
            'price': i
        })
    print(f"data: {COMMODITY_DATA}")

    response: ClientResponse = await session.post(COMMODITY_URL, json=COMMODITY_DATA)
        
    return await response.json()


async def main() -> None:
    cookies: BaseCookie = await login()
    csrftoken: str = await get_csrftoken()

    cookies["csrftoken"] = csrftoken

    async with ClientSession(cookies=cookies, headers={"X-Csrftoken": csrftoken}) as session:
        tasks = [task(session) for i in range(2)]
        res = await asyncio.gather(*tasks)
        print(f"res: {res}")

        
if __name__ == '__main__':
    asyncio.run(main())

