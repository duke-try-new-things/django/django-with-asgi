from __future__ import annotations
from typing import (
    TYPE_CHECKING, Optional, List
)

if TYPE_CHECKING:
    pass

import time
import asyncio
from django.http import HttpRequest
from ninja import Router

from ninja_lib.auths import async_session_auth
from .models import *
from .schema import CommoditySchema
from .orm import (
    get_commodities,
    create_a_commodity,
)


store_router: Router = Router(auth=async_session_auth)


@store_router.api_operation(
    methods=['GET'],
    path='/commodities',
    url_name='get_commodities',
    response=List[CommoditySchema],
)
async def get_commodities_api(request: HttpRequest, commodity_name: Optional[str] = None):
    """ Get commodities.

    :param commodity_name: commodity's name

    :return: commodity list
    """
    await asyncio.sleep(1)

    commodities: List[dict] = await get_commodities(commodity_name)
    print(f"commodities: {commodities}")
    
    return commodities


@store_router.api_operation(
    methods=['POST'],
    path='/commodities',
    url_name='create_commodities',
    response=List[CommoditySchema],
)
async def create_commodities(request:HttpRequest, body: List[CommoditySchema]):
    """ Create commodities.

    It will sleep 3 seconds when create every commodity.  
    To test whether the async functions are working or not.

    :param body: commodities info which need to create

    :return: commodities info
    """
    start_at: float = time.time() 

    tasks = [create_commodity_task(dict(i)) for i in body]
    res: List[Commodity] = await asyncio.gather(*tasks)
    
    end_at: float = time.time() 
    print(f"{len(tasks)} 個任務共花了 {end_at-start_at} 秒")

    return res
    

async def create_commodity_task(create_info: dict) -> None:
    # Let it sleep 3 sec
    await asyncio.sleep(3)
    return await create_a_commodity(create_info)
