import uuid
from django.db import models


class Commodity(models.Model):
    """ 商品 """
    class Meta:
        db_table = 'store_commodity'
        constraints = [
            models.UniqueConstraint(
                fields=('name', 'price'),
                name='name_price_unique'
            )
        ]

    name = models.CharField(max_length=64)
    price = models.IntegerField()

    def __str__(self) -> str:
        return self.name


class Order(models.Model):
    """ 訂單 """
    class Meta:
        db_table = 'store_order'
    
    order_id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    is_paid = models.BooleanField(default=False)

    def __str__(self) -> str:
        return str(self.order_id)


class OrderCommodityMapping(models.Model):
    """ 訂單與商品關聯 """
    class Meta:
        db_table = 'store_order_commodity_mapping'
        constraints = [
            models.UniqueConstraint(
                fields=('order', 'commodity'),
                name='order_commodity_unique'
            )
        ]

    order = models.ForeignKey('Order', on_delete=models.CASCADE)
    commodity = models.ForeignKey('Commodity', on_delete=models.CASCADE)
    commodity_number = models.IntegerField()

