from __future__ import annotations
from typing import (
    TYPE_CHECKING, Optional, List
)

if TYPE_CHECKING:
    from django.db.models import QuerySet

import asyncio
from asgiref.sync import sync_to_async

from .models import *


@sync_to_async
def get_commodities(commodity_name: Optional[str] = None) -> List[Commodity]:
    """ Get all commodities.

    Must use sync_to_async() to decorate the orm, so extract the orm statements from views.

    :param commodity_name: commodity name

    :return: list of commodity model obj, must tranfer to list, because queryset is lazily
    """
    commodities: QuerySet[Commodity] = Commodity.objects.all()

    if commodity_name is not None:
        commodities = commodities.filter(name__contains=commodity_name)

    return list(commodities)  # must tranfer to list


@sync_to_async
def create_a_commodity(create_info: dict):
    return Commodity.objects.create(**create_info)