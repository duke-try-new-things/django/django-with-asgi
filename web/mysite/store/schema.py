from __future__ import annotations
from typing import (
    TYPE_CHECKING
)

if TYPE_CHECKING:
    pass

from ninja.schema import Field
from ninja import ModelSchema

from .models import Commodity


class CommoditySchema(ModelSchema):
    
    class Config:
        model = Commodity
        model_fields = ["name", "price"]
