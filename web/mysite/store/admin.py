from django.contrib import admin
from .models import *


class CommodityAdmin(admin.ModelAdmin):
    list_display = ("name", "price",)

admin.site.register(Commodity, CommodityAdmin)


class OrderAdmin(admin.ModelAdmin):
    list_display = ("order_id", "is_paid",)

admin.site.register(Order, OrderAdmin)


class OrderCommodityMappingAdmin(admin.ModelAdmin):
    list_display = ("order", "commodity", "commodity_number")

admin.site.register(OrderCommodityMapping, OrderCommodityMappingAdmin)
