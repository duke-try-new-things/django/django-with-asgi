from ninja import NinjaAPI
from store.views import store_router
from authentication.views import auth_router


api: NinjaAPI = NinjaAPI(version='1.0.0', csrf=True)

api.add_router('store', store_router, tags=['Store'])
api.add_router('user', auth_router, tags=["Auth"])
