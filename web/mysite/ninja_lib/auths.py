from __future__ import annotations
from typing import (
    TYPE_CHECKING, Optional
)

if TYPE_CHECKING:
    from django.contrib.auth.models import User

from ninja.errors import HttpError
from django.http import HttpRequest
from asgiref.sync import sync_to_async


@sync_to_async
def get_user_from_request(request: HttpRequest) -> Optional[User]:
    return request.user if request.user.is_authenticated else None


async def async_session_auth(request: HttpRequest) -> User:
    """ Custom async authentication.

    因為 Django ninja 默認的認證類都是同步的。

    :return: user model obj
    """
    user: Optional[User] = await get_user_from_request(request)
    if user is None:
        raise HttpError(403, 'Authorization Failed')

    return user