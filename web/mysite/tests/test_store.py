from __future__ import annotations
from typing import (
    TYPE_CHECKING, List
)

if TYPE_CHECKING:
    from django.test import Client
    from store.models import *

import json
from django.urls import reverse

from tests.general import URLS_NAMESPACE, CONTENT_TYPE
from tests.factories.store_factory import *


def test_get_commodities(db, mock_user: Client) -> None:
    pencil: Commodity = CommodityFactory.create(name="鉛筆", price=10)
    ballpoint: Commodity = CommodityFactory.create(name="原子筆", price=30)
    url: str = reverse(f"{URLS_NAMESPACE}:get_commodities")

    response = mock_user.get(url, {"commodity_name": pencil.name})

    assert response.status_code == 200
    assert response.json() == [
        {
            "name": pencil.name,
            "price": pencil.price
        }
    ]


def test_create_commodities(db, mock_user: Client) -> None:
    url: str = reverse(f"{URLS_NAMESPACE}:create_commodities")
    payload: List[dict] = [
        {"name": "test1", "price": 10},
        {"name": "test2", "price": 20},
    ]

    response = mock_user.post(url, json.dumps(payload), content_type=CONTENT_TYPE)

    assert response.status_code == 200
    assert len(response.json()) == len(payload)
