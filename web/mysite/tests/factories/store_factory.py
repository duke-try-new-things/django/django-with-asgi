from __future__ import annotations
from typing import (
    TYPE_CHECKING
)

if TYPE_CHECKING:
    pass

import uuid
import factory
from faker import Factory as FakerFactory
from pytest_factoryboy import register


faker = FakerFactory.create()


class CommodityFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'store.Commodity'
