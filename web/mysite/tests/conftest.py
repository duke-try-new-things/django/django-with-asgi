from __future__ import annotations
from typing import (
    TYPE_CHECKING
)

if TYPE_CHECKING:
    from django.test import Client
    from django.contrib.auth.models import User
    from store.models import *

from pytest import fixture

from tests.factories.store_factory import *


@fixture(name="mock_user")
def create_mock_user(db, client: Client, django_user_model: User) -> Client:
    user: User = django_user_model.objects.create_user(
        username="pytest",
        password="pytest",
        is_superuser=False,
        is_staff=False,
        is_active=True,
    )
    client.force_login(user)
    return client

