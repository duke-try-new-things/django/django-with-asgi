from __future__ import annotations
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    pass


URLS_NAMESPACE: str = "api-1.0.0"
CONTENT_TYPE: str = "application/json"
