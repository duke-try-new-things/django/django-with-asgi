from __future__ import annotations
from typing import (
    TYPE_CHECKING
)

if TYPE_CHECKING:
    pass

from ninja.schema import Field, Schema


class LoginInputSchema(Schema):
    username: str
    password: str

