from __future__ import annotations
from typing import (
    TYPE_CHECKING
)

if TYPE_CHECKING:
    pass

from django.http import HttpRequest
from ninja import Router
from django.contrib import auth
from django.contrib.auth.models import User
from django.middleware.csrf import get_token

from .schema import LoginInputSchema


auth_router: Router = Router()


@auth_router.api_operation(
    methods=["POST"],
    path="/login",
    url_name="login",
    response=dict
)
def login(request: HttpRequest, login_input: LoginInputSchema):
    user: User = auth.authenticate(
        username=login_input.username, 
        password=login_input.password
    )
    if user and user.is_active:
        auth.login(request, user)
        return {"status": "success"}
    
    return {"status": "fail"}


@auth_router.api_operation(
    methods=["GET"],
    path="/csrftoken",
    url_name="get_csrftoken",
    response=str
)
def get_csrftoken(request: HttpRequest):
    csrftoken: str = get_token(request)
    return csrftoken
    

