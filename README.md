# Django with asgi

用於練習使用 Django async api。

## Getting started

所需環境：docker、docker compose

### 1. 啟動容器服務：
``` bash
docker compose up -d
```

### 2. 初始化 Database：
若不能執行 .sh 檔，那直接 .sh 檔中的指令複製出來執行即可。
- migrate the django 
    ``` bash
    bash migrate.sh
    ```
- Insert initail data
    ``` bash
    bash init_db.sh
    ```

### 3. 訪問服務：
因為都有將 Port 透出來，所以直接使用 8888 Port 訪問即可。  
因 api 有綁認證機制，故使用 api 前須先登入 admin（或是使用 login api 登入），測試使用者為 test/test123。
- admin
    ```
    http://127.0.0.1:8888/admin
    ```
- swagger api
    ```
    http://127.0.0.1:8888/api/v1/docs#/
    ```


## Directory Structure

### database/postgresql：
存放用於建立 postgresql 服務的相關資料與 volume 出來的 DB 資料。

### web：
存放用於建立 Django 服務的相關資料。

### secrets：
存放 Django、postgresql 服務中會用到的設定檔。

